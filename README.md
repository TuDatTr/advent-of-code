
#Advent of Code

```
.
├── 2022
│   ├── Cargo.toml
│   ├── common
│   ├── day-1_calorie_counting
│   ├── day-1_calorie_counting-alt
│   ├── day-2_rock_paper_scissors
│   ├── day-2_rock_paper_scissors-alt
│   ├── day-3_rucksack_reorganization
│   └── day-3_rucksack_reorganization-alt
├── create_readme.sh
└── README.md
```

