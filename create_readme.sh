#!/bin/bash

echo -e """
#Advent of Code

\`\`\`
$(tree -L 2 --gitignore | head -n -1)
\`\`\`
""" > README.md
