# Calorie Counting
Implementation of [this task](./task.md) that started clean but ended up in a lot of [bodging](https://en.wiktionary.org/wiki/bodge).

<sup>and is very much not idiomatic rust</sup>

## Usage
`cargo run --release -- --input <Path to file with inputs>`

Alternative
```sh
cargo install --release . 
calorie_counting -i <Path to file with inputs>
```
