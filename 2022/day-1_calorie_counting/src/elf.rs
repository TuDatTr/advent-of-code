use std::fmt;

#[derive(Debug, Clone)]
pub struct Elf {
    _input: String,
    calories: Vec<u64>,
}

impl Elf {
    pub fn new(input: &str) -> Self {
        let cal: Vec<u64> = input
            .split('\n')
            .map(|i| i.parse::<u64>().unwrap())
            .collect();
        Self {
            _input: input.to_string(),
            calories: cal,
        }
    }

    pub fn total_calories(self) -> u64 {
        self.calories.iter().sum()
    }
}

impl fmt::Display for Elf {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Elf: {:?}", self.calories)
    }
}
