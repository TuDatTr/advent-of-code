use common::cli::Cli;

use calorie_counting_lib::{task_1a, task_1b};
use clap::Parser;
use tracing::{info, debug};

pub fn main() {
    tracing_subscriber::fmt::init();
    debug!("Running with DEBUG logging");
    info!("Running with INFO logging");

    let args = Cli::parse();
    debug!("{:#?}", args);

    task_1a(&args.input);
    task_1b(&args.input);
}
