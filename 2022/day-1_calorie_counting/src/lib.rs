use std::path::PathBuf;
use std::collections::BinaryHeap;
use tracing::{debug, info};
use common::read_file;

mod elf;
use crate::elf::Elf;


/// Finds the Elf carrying the most Calories.
/// Outputs how many total Calories that Elf is carrying.
pub fn task_1a(input: &PathBuf) {
    debug!("Running task 1a");
    info!("Running task 1a with {:?}", input);

    let content = read_file(input).unwrap();
    let splits = content.split("\n\n");
    println!("Result (1a): {}", splits.map(|s| Elf::new(s).total_calories()).max().unwrap());
}

/// Finds the top three Elves carrying the most Calories.
/// Outputs how many Calories those Elves are carrying in total.
pub fn task_1b(input: &PathBuf) {
    debug!("Running task 1b");
    info!("Running task 1b with {:?}", input);
    
    let content = read_file(input).unwrap();
    let splits = content.split("\n\n");
    let mut heap = splits.map(|s| Elf::new(s).total_calories()).collect::<BinaryHeap<_>>();

    let mut sum = 0u64;
    for _ in 0..3 {
        if let Some(c) = heap.pop() {
            sum += c;
        }
    }
    println!("Result (1b): {}", sum);
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::remove_file;
    use std::str::from_utf8;
    use common::{create_file, read_file};

    const PATH: &str = "input.txt";
    const CONTENT: &[u8; 54] =
        b"1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000";

    fn create_test_setup() {
        let file_path = &PathBuf::from(PATH);
        let original = CONTENT;
        create_file(file_path, from_utf8(original).unwrap().to_string());
    }

    fn cleanup_test_setup() {
        let _ = remove_file(&PathBuf::from(PATH));
    }
    /// Test if we corretly read file input.
    #[test]
    fn file_io() {
        create_test_setup();
        let content = read_file(&PathBuf::from(PATH));
        cleanup_test_setup();
        assert_eq!(from_utf8(CONTENT).unwrap(), content.unwrap());
    }
}
