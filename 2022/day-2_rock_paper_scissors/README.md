# Calorie Counting
Implementation of [this task](./task.md). I tried to do it idiomatically, but it quickly turned into a mess when part two was introduced. 

<sub>I'm sensing a theme here</sub>


## Usage
`cargo run --release -- --input <Path to file with inputs>`

Alternative
```sh
cargo install --release . 
rock_paper_scissors -i <Path to file with inputs>
```
