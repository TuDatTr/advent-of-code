use std::path::PathBuf;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about=None, long_about = None)]
pub struct Cli {
    /// Path to input file
    #[arg(short, long)]
    pub input: PathBuf
}
