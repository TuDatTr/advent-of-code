use common::read_file;
use std::path::PathBuf;
use tracing::{info, debug};

mod choice;
use crate::choice::Choice;

mod gameresult;
use crate::gameresult::GameResult;

/// Calculates score for single game, given specification from part one of the task.
fn calculate_result_1a(line: &str) -> u64 {
    debug!("Get score for 1a of game {}", line);

    let mut input = line.chars();
    let opponent_choice = match input.next() {
        Some('A') => Choice::Rock,
        Some('B') => Choice::Paper,
        Some('C') => Choice::Scissors,
        _ => Choice::Rock, // Todo: Error handling, this shouldn't happen
    };

    input.next();

    let user_choice = match input.next() {
        Some('X') => Choice::Rock,
        Some('Y') => Choice::Paper,
        Some('Z') => Choice::Scissors,
        _ => Choice::Rock, // Todo: Error handling, this shouldn't happen
    };
    let result = user_choice.play(&opponent_choice);
    info!("Total score (1a): {}", result);
    result
}

/// Calculates score for single game, given specification from part two of the task.
fn calculate_result_1b(line: &str) -> u64 {
    debug!("Get score for 1b of game {}", line);

    let mut input = line.chars();
    let opponent_choice = match input.next() {
        Some('A') => Choice::Rock,
        Some('B') => Choice::Paper,
        Some('C') => Choice::Scissors,
        _ => Choice::Rock, // Todo: Error handling, this shouldn't happen
    };

    input.next();

    let outcome = match input.next() {
        Some('X') => GameResult::Loose,
        Some('Y') => GameResult::Draw,
        Some('Z') => GameResult::Win,
        _ => GameResult::Loose, // Todo: Error handling, this shouldn't happen
    };
    let result = opponent_choice.cheat(&outcome);
    info!("Total score (1b): {}", result);
    result
}

pub fn task_2a(input: &PathBuf) -> u64 {
    let content = read_file(input).unwrap();
    content.split('\n').map(|l| {
        let r = calculate_result_1a(l);
        info!("{}", r);
        r
    }).sum()
}

pub fn task_2b(input: &PathBuf) -> u64 {
    let content = read_file(input).unwrap();
    content.split('\n').map(|l| calculate_result_1b(l)).sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use common::create_file;
    use std::fs::remove_file;

    const PATH: &str = "input.txt";
    const CONTENT: &str = "A Y\nB X\nC Z";

    #[test]
    fn test_task_2a() {
        let test_file = PathBuf::from(PATH);
        create_file(&test_file, CONTENT.to_string());
        let result = task_2a(&test_file);
        let _ = remove_file(&test_file);
        let expected = 15u64;
        
        assert_eq!(result, expected);
    }

    #[test]
    fn test_task_2b() {
        let test_file = PathBuf::from(PATH);
        create_file(&test_file, CONTENT.to_string());
        let result = task_2b(&test_file);
        let _ = remove_file(&test_file);
        let expected = 12u64;
        
        assert_eq!(result, expected);
    }
}
