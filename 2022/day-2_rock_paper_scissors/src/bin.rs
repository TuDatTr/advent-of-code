use rock_paper_scissors_lib::{task_2a, task_2b};
use clap::Parser;
use tracing::debug;

mod cli;
use crate::cli::Cli;

pub fn main() {
    tracing_subscriber::fmt::init();

    let args = Cli::parse();
    debug!("Args: {:?}", args);

    println!("Result (2a): {}", task_2a(&args.input));
    println!("Result (2b): {}", task_2b(&args.input));
}
