use strum_macros::Display;

/// Enum containing the possible outcomes for Rock, Paper, Scissors.
#[derive(Display, Clone, Eq, PartialEq)]
pub enum GameResult {
    Win,
    Loose,
    Draw
}
