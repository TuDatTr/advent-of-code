use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;
use tracing::{debug, info};

pub mod cli;
    
pub fn create_file(path: &Path, content: String) {
    info!("Creating file");
    debug!("Writing file from: {:?}\nWith:\n{}", path, content);

    let mut file = File::create(path).unwrap();
    let _ = file.write_all(content.as_bytes());
}

pub fn read_file(path: &Path) -> std::io::Result<String> {
    info!("Reading file");
    debug!("Reading file from: {:?}", path);

    let mut file = File::open(path)?;
    let mut contents = String::new();
    let _ = file.read_to_string(&mut contents)?;
    Ok(contents.trim().to_string())
}
