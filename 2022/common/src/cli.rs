use clap::Parser;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[command(author, version, about=None, long_about = None)]
pub struct Cli {
    /// Path to the input file.
    #[arg(short, long)]
    pub input: PathBuf,
}
