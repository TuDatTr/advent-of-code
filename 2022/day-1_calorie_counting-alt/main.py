def task_1a():
    return max([sum([int(calorie) for calorie in  elf.split() if calorie]) for elf in open('input.txt', 'r').read().split("\n\n") if elf])

def task_1b():
    return sum(sorted([sum([int(calorie) for calorie in  elf.split() if calorie]) for elf in open('input.txt', 'r').read().split("\n\n") if elf])[-3:])

if __name__ == '__main__':
    print(f"Result (1a): {task_1a()}")
    print(f"Result (1b): {task_1b()}")
