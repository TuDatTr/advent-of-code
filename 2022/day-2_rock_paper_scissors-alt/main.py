def calc(u1, u2):
    if u1 == u2:
        print(f'{chr(u1+ord("A")-1)} {chr(u2+ord("X")-1)} draw')
        return u2 + 3
    elif u1 == (u2 % 3 - 1):
        print(f'{chr(u1+ord("A")-1)} {chr(u2+ord("X")-1)} win')
        return u2 + 6
    else:
        print(f'{chr(u1+ord("A")-1)} {chr(u2+ord("X")-1)} loss')
        return u2 + 0
    
def test_2a(file):
    res = []
    for l in open(file, 'r').read().split('\n'):
        if l:
            a, b = l.split()
            res.append(calc(*(ord(a) - ord("A") + 1, ord(b) - ord("X") + 1)))
    res = sum(res)
    print(res)
    apprehension = task_2a(file)
    assert res == apprehension

    
def task_2a(file):
    numify = lambda a_b: (ord(a_b[0]) - ord("A") + 1, ord(a_b[1]) - ord("X") + 1)
    return sum([calc(*numify(l.split())) for l in open(file, 'r').read().split('\n') if l])

def task_2b(file):
    raise NotImplementedError

if __name__ == '__main__':
    print(task_2a('input.txt'))
    # print(task_2b('input.txt'))

