use common::cli::Cli;
use common::read_file;
use rucksack_reorganization_lib::{task_3a, task_3b};
use tracing::debug;

use clap::Parser;

pub fn main() {
    tracing_subscriber::fmt::init();

    let args = Cli::parse();
    debug!("Args: {:?}", args);

    let content = read_file(&args.input).unwrap();
    println!("Result (3a): {}", task_3a(&content));
    println!("Result (3b): {}", task_3b(&content));
}
