use tracing::{info, debug};

use itertools::Itertools;

mod rucksack;
use crate::rucksack::Rucksack;

/// Gives a list of characters in `first` and `second`, that are in both strings
fn common_chars(first: &str, second: &str) -> Vec<char> {
    // probably not very idiomatic, also I shouldnt use Vec<char> here, instead String
    debug!("Comparing: {:10}, {:10}", first, second);
    let result = first.chars().filter(|c| second.chars().find(|d| d == c).is_some()).collect::<Vec<char>>();
    info!("Common Characters: {:?}", result);
    result
}

/// Converts `input` to it's "priority" based on the tasks specifications
/// a-z -> 01-26 respectivly
/// A-Z -> 27-52 respectivly
fn priority_of(input: &char) -> u64 {
    debug!("Priority of {}", input);
    let base: u32 = 'a'.into();
    let mut priority: u32 = input.to_ascii_lowercase() as u32 - base + 1;
    if input.is_ascii_uppercase() {
        priority += 26;
    }
    let result: u64 = priority.into();
    debug!("Priority {}", &result);
    result
}

/// Solution for 3a
pub fn task_3a (content: &str) -> u64 {
    content.split('\n')
        .map(|c| Rucksack::new(c))
        .map(|c| priority_of(&common_chars(&c.compartments[0], &c.compartments[1])[0]))
        .sum()
}

/// Solution for 3b
pub fn task_3b (content: &str) -> u64 {
    let result: u64 = content.split('\n')
        .map(|c| Rucksack::new(c))
        .chunks(3).into_iter()
        .map(|c| {
            let group = c.map(|r| r.total_content()).collect_vec();
            let common_badge_0_1: String = common_chars(&group[0], &group[1]).into_iter().collect();
            let badge = common_chars(&common_badge_0_1, &group[2])[0];
            priority_of(&badge)
        }).sum();

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const CONTENT: &str = "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw";

    #[test]
    fn test_common_chars() {
        let inputs: Vec<&str> = CONTENT.split('\n').collect();
        let results: Vec<char> = inputs.iter().map(|r| {
            let rucksack = Rucksack::new(r);
            common_chars(&rucksack.compartments[0], &rucksack.compartments[1])[0]
        }).collect();
        let expected: Vec<char> = vec!('p', 'L', 'P', 'v', 't', 's');
        assert_eq!(results, expected);
    }

    #[test]
    fn test_priority_of() {
        let inputs: Vec<char> = vec!('p', 'L', 'P', 'v', 't', 's');
        let results: Vec<u64> = inputs.iter().map(|c| priority_of(c)).collect();
        let expected: Vec<u64> = vec!(16, 38, 42, 22, 20, 19);

        assert_eq!(results, expected);
    }

    #[test]
    fn test_task_3a() {
        let result = task_3a(CONTENT);
        let expected = 157u64;
        
        assert_eq!(result, expected);
    }

    #[test]
    fn test_task_3b() {
        let result = task_3b(CONTENT);
        let expected = 70;
        
        assert_eq!(result, expected);
    }
}
