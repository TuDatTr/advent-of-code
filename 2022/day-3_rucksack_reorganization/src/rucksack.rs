use tracing::{info, debug};

const COMPARTMENT_COUNT: usize = 2;

#[derive(Debug, Clone)]
pub struct Rucksack {
    pub compartments: [String; COMPARTMENT_COUNT],
}

impl Rucksack {
    /// Splits the `total_content` by half and puts it in their respective compartments
    pub fn new(total_content: &str) -> Self {
        debug!("Rucksack with {}", total_content);
        let split_position = total_content.len()/2;
        let (left, right) = total_content.split_at(split_position.into());
        let content = [left.to_string(), right.to_string()];
        
        debug!("Rucksack with {:?}", content);
        Rucksack {
            compartments: content,
        }
    }

    /// Returns the content of the rucksack as a single String
    pub fn total_content(&self) -> String {
        info!("");
        self.compartments.concat().to_string()
    }
}
