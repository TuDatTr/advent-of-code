# Calorie Counting
Implementation of [this task](./task.md). 

This time I ran with this implementation second, since it seemed easier to quickly implement in Python.
Implementing it in Python first, allowed me to have a more organized approach to the task, given that I had already completed it once.
I may continue doing this going forward.

## Usage
`cargo run --release -- --input <Path to file with inputs>`

Alternative
```sh
cargo install --release . 
rucksack_reorganization -i <Path to file with inputs>
```
