def task3a():
    sum = 0
    for rucksack in open('input.txt', 'r').read().splitlines():
        left = rucksack[:int(len(rucksack)/2)]
        right= rucksack[int(len(rucksack)/2):]
        
        common = [c for c in left if c in right][0] 
        priority = ord(common) - ord('a') + 1  if common.islower() else ord(common) - ord('A') + 27 
        # print(f"{common}: {priority}")
        sum += priority
    return sum
print(f'Task 3a: {task3a()}')

def task3b():
    sum  = 0
    lines = open('input.txt', 'r').read().splitlines()
    for rucksacks in [lines[n:n+3] for n in range(0, len(lines), 3)]:
        common = [d for d in [c for c in rucksacks[0] if c in rucksacks[1]] if d in rucksacks[2]][0]
        priority = ord(common) - ord('a') + 1  if common.islower() else ord(common) - ord('A') + 27 
        # print(f"{common}: {priority}")
        sum += priority
    return sum

print(f'Task 3b: {task3b()}')


